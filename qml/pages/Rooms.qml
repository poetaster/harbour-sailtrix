import QtQuick 2.0
import Sailfish.Silica 1.0
import RoomsBackend 1.0
import "../custom"
import "../sf-docked-tab-bar"

Page {
    id: page

    allowedOrientations: Orientation.All

    property string active_room;
    property int notifs;

    objectName: "Rooms"

    RoomsBackend {
        id: rooms

        onNotifCountChanged: notifs = notifCount
    }



    Connections {
        target: sailtrixSignals
        onRoomOpenCommand: {
            console.log("received room open command!");
            if (pageStack.currentPage.room_id !== room_id) {
                pageStack.push("Messages.qml", { room_id: room_id, room_name: rooms.get_name(room_id) });
            }
        }
        onJoinRoomOpenCommand: {
            console.log("received join room command");
            pageStack.push("JoinPublicRoom.qml", { room_id: room_id, name: display_name})
        }
        onUserOpenCommand: {
            console.log("received user open command");
            pageStack.push("User.qml", { user_id: user_id, display_name: display_name})
        }
    }

    Component {
        id: sectionHeading

        PageHeader {
            title: section
            id: header
        }
    }

    SlideshowView {
        id: viewsSlideshow
        anchors.fill: parent
        itemWidth: width
        itemHeight: height

        clip: true
        currentIndex: tabBar.currentSelection

        model: VisualItemModel {
            Item {
                width: parent.width
                height: parent.height

                RoomsDisplay {
                    id: favorites
                    model: rooms.favorites
                    type: "Favorites"
                }

                ViewPlaceholder {
                    enabled: favorites.count == 0
                    text: qsTr("No Favorites")
                    hintText: "Add favorites by long-pressing rooms"
                }
            }

            Item {
                width: parent.width
                height: parent.height
                RoomsDisplay {
                    id: reg_rooms
                    model: rooms.regularRooms
                    type: "Rooms"
                }

                ViewPlaceholder {
                    enabled: reg_rooms.count == 0
                    text: qsTr("No Rooms")
                    hintText: "Join some rooms"
                }
            }

            Item {
                width: parent.width
                height: parent.height
                RoomsDisplay {
                    id: direct_rooms
                    model: rooms.directRooms
                    type: "Direct Messages"
                }

                ViewPlaceholder {
                    enabled: direct_rooms.count == 0
                    text: qsTr("No Direct Messages")
                }
            }

            Item {
                width: parent.width
                height: parent.height
                RoomsDisplay {
                    id: invites_display
                    model: rooms.invites
                    type: "Invites"
                }

                ViewPlaceholder {
                    enabled: invites_display.count == 0
                    text: qsTr("No Invites")
                }
            }
        }

        onCurrentIndexChanged: {
            tabBar.currentSelection = currentIndex
        }

        Connections {
            target: tabBar
            onCurrentSelectionChanged: {
                console.log("Selection changed to " + tabBar.currentSelection)
                if (viewsSlideshow.currentIndex !== tabBar.currentSelection) {
                    viewsSlideshow.positionViewAtIndex(tabBar.currentSelection, PathView.SnapPosition);
                }
            }
        }

    }



    PageBusyIndicator {
        running: !rooms.done
    }

    Component.onCompleted: rooms.load();

    onStatusChanged: {
        console.log(status);
        if (status == PageStatus.Inactive) {
            rooms.pause();
        } else if (status == PageStatus.Active) {
            rooms.reloadSettings();
            if (rooms.loaded()) {
                rooms.mark_read(active_room);
                active_room = "";
                rooms.resume();
            }
            else
                rooms.load();
        }
    }
}
