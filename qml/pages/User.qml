import QtQuick 2.0
import Sailfish.Silica 1.0
import UserBackend 1.0;
import QtGraphicalEffects 1.0

Page {
    id: page

    allowedOrientations: Orientation.All

    property string user_id;
    property string display_name;
    property string avatar_url;
    property bool display_ignore: true;
    property Page prev_page;
    property bool running;

    UserBackend {
        id: backend
        onIs_doneChanged: {
            if (blockButton.visible) {
                blockButton.visible = false;
                unblockButton.visible = true;
            } else {
                blockButton.visible = true;
                unblockButton.visible = false;
            }
        }

        onNew_room_idChanged: {
            pageStack.push("Messages.qml", { room_name: "Direct Message with " + user_id, room_id: new_room_id})
        }
    }

    Column {
        id: column
        width: page.width
        height: page.height;
        spacing: Theme.paddingLarge

        PageHeader {
            title: display_name ? display_name : user_id
        }

        Image {
            id: icon
            height: Theme.itemSizeHuge
            width: height

            source: avatar_url ? avatar_url : "image://theme/icon-m-media-artists"
            anchors.horizontalCenter: parent.horizontalCenter

            onStatusChanged: {
                if (status == Image.Error) {
                    source = "image://theme/icon-m-media-artists";
                }
            }

            fillMode: Image.PreserveAspectCrop

            layer.enabled: true
            layer.effect: OpacityMask {
                maskSource: Item {
                    width: Theme.itemSizeHuge
                    height: width
                    Rectangle {
                        radius: parent.width
                        anchors.centerIn: parent
                        width: parent.width
                        height: parent.height
                    }
                }
            }
        }

        Label {
            text: user_id
            anchors.horizontalCenter: parent.horizontalCenter
            color: Theme.primaryColor
        }

        Button {
            text: qsTr("Direct Message")
            preferredWidth: Theme.buttonWidthLarge
            anchors.horizontalCenter: parent.horizontalCenter
            onClicked: {
                running = true;
                backend.start_dm(user_id);
            }
        }

        Button {
            id: blockButton
            text: qsTr("Ignore")
            preferredWidth: Theme.buttonWidthLarge
            anchors.horizontalCenter: parent.horizontalCenter
            backgroundColor: Theme.errorColor
            onClicked: backend.block(user_id)
            visible: display_ignore && !backend.is_blocked(user_id);
        }

        Button {
            id: unblockButton
            text: qsTr("Unignore")
            preferredWidth: Theme.buttonWidthLarge
            anchors.horizontalCenter: parent.horizontalCenter
            visible: backend.is_blocked(user_id)
            onClicked: backend.unblock(user_id)
        }

        BusyIndicator {
            size: BusyIndicatorSize.Large
            id: indicate
            anchors.centerIn: parent
            running: running
            visible: running
        }
    }
}
