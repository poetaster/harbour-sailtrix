# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = harbour-sailtrix

CONFIG += sailfishapp

SOURCES += \
    olm/lib/crypto-algorithms/aes.c \
    olm/lib/crypto-algorithms/arcfour.c \
    olm/lib/crypto-algorithms/base64.c \
    olm/lib/crypto-algorithms/blowfish.c \
    olm/lib/crypto-algorithms/des.c \
    olm/lib/crypto-algorithms/md2.c \
    olm/lib/crypto-algorithms/md5.c \
    olm/lib/crypto-algorithms/rot-13.c \
    olm/lib/crypto-algorithms/sha1.c \
    olm/lib/crypto-algorithms/sha256.c \
    olm/lib/curve25519-donna/curve25519-donna.c \
    olm/lib/ed25519/src/add_scalar.c \
    olm/lib/ed25519/src/fe.c \
    olm/lib/ed25519/src/ge.c \
    olm/lib/ed25519/src/key_exchange.c \
    olm/lib/ed25519/src/keypair.c \
    olm/lib/ed25519/src/sc.c \
    olm/lib/ed25519/src/seed.c \
    olm/lib/ed25519/src/sha512.c \
    olm/lib/ed25519/src/sign.c \
    olm/lib/ed25519/src/verify.c \
    olm/src/account.cpp \
    olm/src/base64.cpp \
    olm/src/cipher.cpp \
    olm/src/crypto.cpp \
    olm/src/ed25519.c \
    olm/src/error.c \
    olm/src/inbound_group_session.c \
    olm/src/megolm.c \
    olm/src/memory.cpp \
    olm/src/message.cpp \
    olm/src/olm.cpp \
    olm/src/outbound_group_session.c \
    olm/src/pickle.cpp \
    olm/src/pickle_encoding.c \
    olm/src/pk.cpp \
    olm/src/ratchet.cpp \
    olm/src/sas.c \
    olm/src/session.cpp \
    olm/src/utility.cpp \
    src/createroombackend.cpp \
    src/dbus/dbusactivation.cpp \
    src/dbus/notifications.cpp \
    src/dbus/sailtrixadapter.cpp \
    src/dbus/sailtrixsignals.cpp \
    src/enc-util.cpp \
    src/harbour-sailtrix.cpp \
    src/invitebackend.cpp \
    src/loginbridge.cpp \
    src/message.cpp \
    src/messages.cpp \
    src/messagesmodel.cpp \
    src/picturedisplaybackend.cpp \
    src/roomdirectorybackend.cpp \
    src/roomdirectorymodel.cpp \
    src/rooms.cpp \
    src/roomsmodel.cpp \
    src/settingsbackend.cpp \
    src/userbackend.cpp

DISTFILES += \
    COPYING \
    README \
    harbour-sailtrix.desktop \
    html/ssoDone.html \
    icons/108x108/harbour-sailtrix.png \
    icons/128x128/harbour-sailtrix.png \
    icons/172x172/harbour-sailtrix.png \
    icons/86x86/harbour-sailtrix.png \
    olm/CHANGELOG.rst \
    olm/CMakeLists.txt \
    olm/CONTRIBUTING.md \
    olm/LICENSE \
    olm/cmake/OlmConfig.cmake.in \
    olm/common.mk \
    olm/docs/DH ratchet.svg \
    olm/docs/DH ratchet.txt \
    olm/docs/double_ratchet.dia \
    olm/docs/double_ratchet.svg \
    olm/docs/megolm.md \
    olm/docs/olm.md \
    olm/docs/signing.md \
    olm/exports.py \
    olm/include/module.modulemap \
    olm/jenkins.sh \
    olm/lib/crypto-algorithms/README.md \
    olm/lib/curve25519-donna/LICENSE.md \
    olm/lib/curve25519-donna/README \
    olm/lib/curve25519-donna/contrib/Curve25519Donna.java \
    olm/lib/curve25519-donna/contrib/make-snippets \
    olm/lib/curve25519-donna/curve25519-donna.podspec \
    olm/lib/curve25519-donna/test-sc-curve25519.s \
    olm/lib/ed25519/ed25519_32.dll \
    olm/lib/ed25519/ed25519_64.dll \
    olm/lib/ed25519/readme.md \
    olm/olm.pc.in \
    olm/olm.pri \
    qml/cover/CoverPage.qml \
    qml/cover/LoggedInCover.qml \
    qml/custom/Audio.qml \
    qml/custom/RoomsDisplay.qml \
    qml/harbour-sailtrix.qml \
    qml/logged-in.qml \
    qml/pages/CreateRoom.qml \
    qml/pages/Credits.qml \
    qml/pages/Invite.qml \
    qml/pages/JoinPublicRoom.qml \
    qml/pages/LoginDialog.qml \
    qml/pages/LoginWaiting.qml \
    qml/pages/MessageSource.qml \
    qml/pages/Messages.qml \
    qml/pages/PictureDisplay.qml \
    qml/pages/RoomDirectory.qml \
    qml/pages/Rooms.qml \
    qml/pages/SSOLogin.qml \
    qml/pages/Settings.qml \
    qml/pages/Share.qml \
    qml/pages/Start.qml \
    qml/pages/User.qml \
    qml/pages/VideoPage.qml \
    rpm/harbour-sailtrix.changes \
    rpm/harbour-sailtrix.in \
    rpm/harbour-sailtrix.spec \
    rpm/harbour-sailtrix.yaml \
    translations/*.ts

SAILFISHAPP_ICONS = 86x86 108x108 128x128 172x172

# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n

HEADERS += \
    olm/fuzzers/include/fuzzing.hh \
    olm/include/olm/account.hh \
    olm/include/olm/base64.h \
    olm/include/olm/base64.hh \
    olm/include/olm/cipher.h \
    olm/include/olm/crypto.h \
    olm/include/olm/error.h \
    olm/include/olm/inbound_group_session.h \
    olm/include/olm/list.hh \
    olm/include/olm/megolm.h \
    olm/include/olm/memory.h \
    olm/include/olm/memory.hh \
    olm/include/olm/message.h \
    olm/include/olm/message.hh \
    olm/include/olm/olm.h \
    olm/include/olm/olm.hh \
    olm/include/olm/outbound_group_session.h \
    olm/include/olm/pickle.h \
    olm/include/olm/pickle.hh \
    olm/include/olm/pickle_encoding.h \
    olm/include/olm/pk.h \
    olm/include/olm/ratchet.hh \
    olm/include/olm/sas.h \
    olm/include/olm/session.hh \
    olm/include/olm/utility.hh \
    olm/lib/crypto-algorithms/aes.h \
    olm/lib/crypto-algorithms/arcfour.h \
    olm/lib/crypto-algorithms/base64.h \
    olm/lib/crypto-algorithms/blowfish.h \
    olm/lib/crypto-algorithms/des.h \
    olm/lib/crypto-algorithms/md2.h \
    olm/lib/crypto-algorithms/md5.h \
    olm/lib/crypto-algorithms/rot-13.h \
    olm/lib/crypto-algorithms/sha1.h \
    olm/lib/crypto-algorithms/sha256.h \
    olm/lib/curve25519-donna.h \
    olm/lib/ed25519/src/ed25519.h \
    olm/lib/ed25519/src/fe.h \
    olm/lib/ed25519/src/fixedint.h \
    olm/lib/ed25519/src/ge.h \
    olm/lib/ed25519/src/precomp_data.h \
    olm/lib/ed25519/src/sc.h \
    olm/lib/ed25519/src/sha512.h \
    src/createroombackend.h \
    src/dbus/dbusactivation.h \
    src/dbus/notifications.h \
    src/dbus/sailtrixadapter.h \
    src/dbus/sailtrixsignals.h \
    src/enc-util.h \
    src/invitebackend.h \
    src/loginbridge.h \
    src/message.h \
    src/messages.h \
    src/messagesmodel.h \
    src/picturedisplaybackend.h \
    src/roomdirectorybackend.h \
    src/roomdirectorymodel.h \
    src/rooms.h \
    src/roomsmodel.h \
    src/settingsbackend.h \
    src/userbackend.h


INCLUDEPATH += olm/include
INCLUDEPATH += olm/lib

DEFINES += "OLMLIB_VERSION_MAJOR=3"
DEFINES += "OLMLIB_VERSION_MINOR=2"
DEFINES += "OLMLIB_VERSION_PATCH=4"

CONFIG += object_parallel_to_source
QT += dbus


# QMAKE_CXXFLAGS += -fsanitize=address
# QMAKE_LFLAGS += -fsanitize=address
PKGCONFIG += sailfishsecrets sailfishcrypto keepalive nemonotifications-qt5 qt5embedwidget

html.files = html/*
html.path = $$INSTALL_ROOT/usr/share/harbour-sailtrix/html

INSTALLS += html
