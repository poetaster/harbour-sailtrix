<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>CreateRoom</name>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="51"/>
        <source>Create Room</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="52"/>
        <source>Create</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="57"/>
        <source>Visibility</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="59"/>
        <source>Private</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="60"/>
        <source>Public</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="71"/>
        <location filename="../qml/pages/CreateRoom.qml" line="72"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="80"/>
        <source>Topic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="81"/>
        <source>Topic (optional)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="102"/>
        <location filename="../qml/pages/CreateRoom.qml" line="103"/>
        <source>Room alias</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="120"/>
        <source>Enable end-to-end encryption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="121"/>
        <source>Bridges and most bots won&apos;t work yet.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Credits</name>
    <message>
        <location filename="../qml/pages/Credits.qml" line="10"/>
        <source>Credits</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Invite</name>
    <message>
        <location filename="../qml/pages/Invite.qml" line="15"/>
        <source>Invite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Invite.qml" line="35"/>
        <source>You&apos;ve been invited to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Invite.qml" line="35"/>
        <source> chat with</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Invite.qml" line="51"/>
        <source>Join</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Invite.qml" line="65"/>
        <source>Reject</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>JoinPublicRoom</name>
    <message>
        <location filename="../qml/pages/JoinPublicRoom.qml" line="19"/>
        <source>Join Room</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/JoinPublicRoom.qml" line="20"/>
        <source>Join</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/JoinPublicRoom.qml" line="78"/>
        <source> members</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoggedInCover</name>
    <message>
        <location filename="../qml/cover/LoggedInCover.qml" line="43"/>
        <source> notifications</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoginDialog</name>
    <message>
        <location filename="../qml/pages/LoginDialog.qml" line="28"/>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginDialog.qml" line="32"/>
        <source>Homeserver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginDialog.qml" line="33"/>
        <source>Homeserver URL (matrix.org)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginDialog.qml" line="42"/>
        <source>Username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginDialog.qml" line="43"/>
        <source>Username (user1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginDialog.qml" line="52"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MessageSource</name>
    <message>
        <location filename="../qml/pages/MessageSource.qml" line="16"/>
        <source>View Source</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Messages</name>
    <message>
        <location filename="../qml/pages/Messages.qml" line="37"/>
        <location filename="../qml/pages/Messages.qml" line="40"/>
        <source>File saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="38"/>
        <source>File saved to Downloads directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="130"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="136"/>
        <source>Reply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="139"/>
        <source>Replying to </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="152"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="170"/>
        <source>View Source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="175"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="269"/>
        <source>Download audio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="301"/>
        <location filename="../qml/pages/Messages.qml" line="321"/>
        <source>View video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="301"/>
        <source>Download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="330"/>
        <source>Download file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="423"/>
        <source>Message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="424"/>
        <source>Send message to room</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="454"/>
        <source>New Messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="475"/>
        <source>Send file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/messages.cpp" line="1294"/>
        <source> invited </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/messages.cpp" line="1298"/>
        <source> changed their profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/messages.cpp" line="1300"/>
        <source> joined</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PictureDisplay</name>
    <message>
        <location filename="../qml/pages/PictureDisplay.qml" line="17"/>
        <location filename="../qml/pages/PictureDisplay.qml" line="20"/>
        <source>Image saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/PictureDisplay.qml" line="18"/>
        <source>Image saved to Pictures directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/PictureDisplay.qml" line="26"/>
        <location filename="../qml/pages/PictureDisplay.qml" line="29"/>
        <source>Image error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/PictureDisplay.qml" line="27"/>
        <source>Image not saved to Pictures directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/PictureDisplay.qml" line="51"/>
        <source>Save to Pictures</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/PictureDisplay.qml" line="59"/>
        <source>Share</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RoomDirectory</name>
    <message>
        <location filename="../qml/pages/RoomDirectory.qml" line="28"/>
        <source>Explore public rooms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomDirectory.qml" line="33"/>
        <source>Find a room...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomDirectory.qml" line="46"/>
        <source>Search in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomDirectory.qml" line="49"/>
        <source>My homeserver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomDirectory.qml" line="50"/>
        <source>Matrix.org</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomDirectory.qml" line="60"/>
        <source>No results</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomDirectory.qml" line="61"/>
        <source>Please try a different search term</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomDirectory.qml" line="71"/>
        <source>Search for a room</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomDirectory.qml" line="72"/>
        <source>Use the search box to search for a room</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Rooms</name>
    <message>
        <location filename="../qml/pages/Rooms.qml" line="74"/>
        <source>No Favorites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Rooms.qml" line="90"/>
        <source>No Rooms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Rooms.qml" line="106"/>
        <source>No Direct Messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Rooms.qml" line="121"/>
        <source>No Invites</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RoomsDisplay</name>
    <message>
        <location filename="../qml/custom/RoomsDisplay.qml" line="42"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/custom/RoomsDisplay.qml" line="47"/>
        <source>Explore public rooms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/custom/RoomsDisplay.qml" line="53"/>
        <source>Create new room</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/custom/RoomsDisplay.qml" line="77"/>
        <source>Remove from Favorites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/custom/RoomsDisplay.qml" line="77"/>
        <source>Favorite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/custom/RoomsDisplay.qml" line="89"/>
        <source>Leave</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SSOLogin</name>
    <message>
        <location filename="../qml/pages/SSOLogin.qml" line="36"/>
        <source>Login with SSO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SSOLogin.qml" line="40"/>
        <source>Homeserver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SSOLogin.qml" line="41"/>
        <source>Homeserver URL (matrix.org)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../qml/pages/Settings.qml" line="56"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="60"/>
        <source>Notifications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="65"/>
        <source>Enable notifications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="72"/>
        <source>Enable notifications when app is closed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="80"/>
        <source>Notification checking interval</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="82"/>
        <source>30 seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="83"/>
        <source>2.5 minutes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="84"/>
        <source>5 minutes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="85"/>
        <source>10 minutes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="86"/>
        <source>15 minutes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="87"/>
        <source>30 minutes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="88"/>
        <source>1 hour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="89"/>
        <source>2 hours</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="90"/>
        <source>4 hours</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="91"/>
        <source>8 hours</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="92"/>
        <source>10 hours</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="93"/>
        <source>12 hours</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="94"/>
        <source>24 hours</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="104"/>
        <source>Display</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="109"/>
        <source>Sort rooms by</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="111"/>
        <source>Activity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="112"/>
        <source>Alphabetical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="122"/>
        <source>Display user avatars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="129"/>
        <source>Global</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="133"/>
        <source>Clear cache</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="134"/>
        <source>Cleared cache</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="138"/>
        <source>Logout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="140"/>
        <source>Logging out</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Share</name>
    <message>
        <location filename="../qml/pages/Share.qml" line="13"/>
        <source>Share</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Start</name>
    <message>
        <location filename="../qml/pages/Start.qml" line="22"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Start.qml" line="40"/>
        <source>Welcome</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Start.qml" line="44"/>
        <source>Log in with Username/Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Start.qml" line="52"/>
        <source>Log in with SSO</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>User</name>
    <message>
        <location filename="../qml/pages/User.qml" line="83"/>
        <source>Direct Message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/User.qml" line="94"/>
        <source>Ignore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/User.qml" line="104"/>
        <source>Unignore</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VideoPage</name>
    <message>
        <location filename="../qml/pages/VideoPage.qml" line="17"/>
        <location filename="../qml/pages/VideoPage.qml" line="20"/>
        <source>Video saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/VideoPage.qml" line="18"/>
        <source>Video saved to Videos directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/VideoPage.qml" line="26"/>
        <location filename="../qml/pages/VideoPage.qml" line="29"/>
        <source>Video error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/VideoPage.qml" line="27"/>
        <source>Video not saved to Videos directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/VideoPage.qml" line="57"/>
        <source>Share</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
