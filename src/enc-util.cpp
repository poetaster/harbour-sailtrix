#include <QString>
#include <QJsonValue>
#include <QJsonObject>
#include <QFile>
#include <QStandardPaths>
#include <QJsonDocument>
#include <QJsonArray>
#include <QDebug>
#include <olm/olm.hh>
#include <QUuid>
#include <Sailfish/Secrets/secret.h>
#include <Sailfish/Secrets/secretmanager.h>
#include <Sailfish/Secrets/createcollectionrequest.h>
#include <Sailfish/Secrets/storesecretrequest.h>
#include <Sailfish/Secrets/storedsecretrequest.h>
#include <Sailfish/Secrets/deletesecretrequest.h>
#include <Sailfish/Secrets/deletecollectionrequest.h>
#include <Sailfish/Secrets/result.h>
#include <Sailfish/Crypto/key.h>
#include <Sailfish/Crypto/keyderivationparameters.h>
#include <Sailfish/Crypto/generatestoredkeyrequest.h>
#include <Sailfish/Crypto/storedkeyrequest.h>
#include <Sailfish/Crypto/result.h>
#include <Sailfish/Crypto/generateinitializationvectorrequest.h>
#include <Sailfish/Crypto/encryptrequest.h>
#include <Sailfish/Crypto/decryptrequest.h>
#include <Sailfish/Crypto/generatekeyrequest.h>
#include <Sailfish/Crypto/generaterandomdatarequest.h>
#include "enc-util.h"
using namespace Sailfish;

/* Memory allocation checked by: @HengYeDev */

QString bad_encrypt_str(QString sender, QString reason = QString("Unknown reason")) {
    return QString("{ \"type\": \"m.room.message\", \"content\": { \"msgtype\": \"m.bad_encrypt\", \"body\": \"⚠️ Unable to decrypt: " + reason + "\" }, \"sender\": \"" + sender + "\" }");
}

bool check_olm(QString decrypted, QString sender_user_id, QString m_user_id, QString my_real_key) {
    QJsonDocument doc = QJsonDocument::fromJson(decrypted.toUtf8());
    if (sender_user_id != doc.object().value("sender").toString()
            || m_user_id != doc.object().value("recipient").toString()
            || my_real_key != doc.object().value("recipient_keys").toObject().value("ed25519").toString()) {
        return false;
    }

    return true;
}

/* bool check_olm_no_verify(QString decrypted, QString sender_user_id, QString m_user_id) {
    QJsonDocument doc = QJsonDocument::fromJson(decrypted.toUtf8());
    if (sender_user_id != doc.object().value("sender").toString()
            || m_user_id != doc.object().value("recipient").toString()) {
        return false;
    }

    return true;
} */

QString decrypt(QJsonValue encrypted, QString algorithm, QString sender_key, QString device_id, QString session_id, QString m_curve25519_id, QJsonObject* olm_sessions, QJsonObject* megolm_sessions, QString m_user_id, OlmAccount* account, QString sender, unsigned int* message_index) {
    if (algorithm == QString("m.olm.v1.curve25519-aes-sha2")) {
        QJsonObject obj = encrypted.toObject();
        QJsonObject mapped = obj.value(m_curve25519_id).toObject();
        int type = mapped.value("type").toInt();
        QString body = mapped.value("body").toString();

        if (type == 0) {
            qDebug() << "Type 0";
            if (olm_sessions->contains(sender_key)) {
                int at = 0;
                for (QJsonValue value : olm_sessions->value(sender_key).toObject().value("sessions").toArray()) {
                    QString pickled_session = value.toString();
                    OlmSession* session = (OlmSession*) malloc(olm_session_size());

                    if (session == nullptr) {
                        return bad_encrypt_str(sender, "Cannot allocate memory");
                    }

                    session = olm_session(session);
                    size_t status = olm_unpickle_session(session, m_user_id.toUtf8().data(), m_user_id.toUtf8().length(), pickled_session.toUtf8().data(), pickled_session.toUtf8().length());

                    if (status != olm_error()) {
                        size_t is_this_session = olm_matches_inbound_session(session, body.toUtf8().data(), body.toUtf8().length());
                        if (is_this_session == 1) {
                            size_t decrypted_buffer_length = olm_decrypt_max_plaintext_length(session, 0, body.toUtf8().data(), body.toUtf8().length());
                            if (decrypted_buffer_length == olm_error()) {
                                qWarning() << olm_session_last_error(session);
                                continue;
                            }
                            char* decrypted_buffer = (char*) malloc(decrypted_buffer_length);

                            if (decrypted_buffer == nullptr) {
                                return bad_encrypt_str(sender, "Cannot decrypt: cannot allocate memory");
                            }
                            size_t str_len = olm_decrypt(session, 0, body.toUtf8().data(), body.toUtf8().length(), decrypted_buffer, decrypted_buffer_length);

                            if (str_len == olm_error()) {
                                qWarning() << olm_session_last_error(session);
                                continue;
                            }
                            // save the session
                            char* session_pickle = (char*) malloc(olm_pickle_session_length(session));

                            if (session_pickle == nullptr) {
                                return bad_encrypt_str(sender, "Cannot pickle: cannot allocate memory");
                            }

                            size_t pickle_length = olm_pickle_session(session, m_user_id.toUtf8().data(), m_user_id.toUtf8().length(), session_pickle, olm_pickle_session_length(session));


                            QJsonArray arr = olm_sessions->value(sender_key).toObject().value("sessions").toArray();
                            arr.insert(at, QString::fromUtf8(session_pickle, pickle_length));

                            QJsonObject obj = olm_sessions->value(sender_key).toObject();
                            obj.insert("sessions", arr);

                            olm_sessions->insert(sender_key, obj);

                            QString decrypted_str = QString::fromUtf8(decrypted_buffer, str_len);

                            free(session);
                            free(session_pickle);
                            free(decrypted_buffer);

                            return decrypted_str;
                        } else {
                            qDebug() << olm_session_last_error(session);
                        }

                        at++;
                    } else {
                        free(session);
                        return bad_encrypt_str(sender, "Cannot unpickle session");
                    }

                }

                OlmSession* session = (OlmSession*) malloc(olm_session_size());

                if (session == nullptr) {
                    return bad_encrypt_str(sender, "Cannot allocate memory");
                }

                session = olm_session(session);
                size_t result = olm_create_inbound_session_from(session, account, sender_key.toUtf8().data(), sender_key.toUtf8().length(), body.toUtf8().data(), body.toUtf8().length());
                if (result != olm_error()) {
                    size_t result = olm_remove_one_time_keys(account, session);
                    if (result != olm_error()) {
                        size_t decrypted_buffer_length = olm_decrypt_max_plaintext_length(session, 0, body.toUtf8().data(), body.toUtf8().length());
                        char* decrypted_buffer = (char*) malloc(decrypted_buffer_length);

                        if (decrypted_buffer == nullptr) {
                            return bad_encrypt_str(sender, "Cannot allocate memory");
                        }

                        size_t str_len = olm_decrypt(session, 0, body.toUtf8().data(), body.toUtf8().length(), decrypted_buffer, decrypted_buffer_length);

                        if (str_len == olm_error()) {
                            qWarning() << olm_session_last_error(session);
                            return bad_encrypt_str(sender, olm_session_last_error(session));
                        }


                        // save the session
                        char* session_pickle = (char*) malloc(olm_pickle_session_length(session));

                        if (session_pickle == nullptr) {
                            return bad_encrypt_str(sender, "Cannot allocate memory for session pickle");
                        }

                        size_t pickle_len = olm_pickle_session(session, m_user_id.toUtf8().data(), m_user_id.toUtf8().length(), session_pickle, olm_pickle_session_length(session));

                        QJsonArray arr = olm_sessions->value(sender_key).toObject().value("sessions").toArray();
                        arr.append(QString::fromUtf8(session_pickle, pickle_len));
                        QJsonObject sessions_obj = olm_sessions->value(sender_key).toObject();
                        sessions_obj.insert("sessions", arr);
                        olm_sessions->insert(sender_key, sessions_obj);

                        QString decrypted_str = QString::fromUtf8(decrypted_buffer, str_len);

                        free(session);
                        free(session_pickle);
                        free(decrypted_buffer);

                        return decrypted_str;
                    } else {
                        QString session_error = olm_account_last_error(account);
                        free(session);
                        return bad_encrypt_str(sender, session_error);
                    }
                } else {
                    QString session_error = olm_session_last_error(session);
                    free(session);
                    return bad_encrypt_str(sender, session_error);
                }

                free(session);
                return bad_encrypt_str(sender);

            } else {
                OlmSession* session = (OlmSession*) malloc(olm_session_size());

                if (session == nullptr) {
                    return bad_encrypt_str(sender, "cannot allocate memory for session");
                }

                session = olm_session(session);
                size_t result = olm_create_inbound_session_from(session, account, sender_key.toUtf8().data(), sender_key.toUtf8().length(), body.toUtf8().data(), body.toUtf8().length());
                if (result != olm_error()) {
                    size_t result = olm_remove_one_time_keys(account, session);
                    if (result != olm_error()) {
                        size_t decrypted_buffer_length = olm_decrypt_max_plaintext_length(session, 0, body.toUtf8().data(), body.toUtf8().length());
                        char* decrypted_buffer = (char*) malloc(decrypted_buffer_length);

                        if (decrypted_buffer == nullptr) {
                            free(session);
                            return bad_encrypt_str(sender, "Cannot allocate memory");
                        }

                        size_t str_len = olm_decrypt(session, 0, body.toUtf8().data(), body.toUtf8().length(), decrypted_buffer, decrypted_buffer_length);

                        if (str_len == olm_error()) {
                            QString err = olm_session_last_error(session);
                            free(session);
                            return bad_encrypt_str(sender, err);
                        }

                        // save the session
                        char* session_pickle = (char*) malloc(olm_pickle_session_length(session));

                        if (session_pickle == nullptr) {
                            free(session);
                            return bad_encrypt_str(sender, "Cannot allocate memory");
                        }

                        size_t pickle_len = olm_pickle_session(session, m_user_id.toUtf8().data(), m_user_id.toUtf8().length(), session_pickle, olm_pickle_session_length(session));

                        QJsonArray arr = olm_sessions->value(sender_key).toObject().value("sessions").toArray();
                        arr.append(QString::fromUtf8(session_pickle, pickle_len));
                        QJsonObject sessions_obj = olm_sessions->value(sender_key).toObject();
                        sessions_obj.insert("sessions", arr);
                        olm_sessions->insert(sender_key, sessions_obj);

                        QString d_str = QString::fromUtf8(decrypted_buffer, str_len);

                        free(session_pickle);
                        free(session);
                        free(decrypted_buffer);

                        return d_str;
                    } else {
                        QString error = olm_account_last_error(account);
                        free(session);
                        return bad_encrypt_str(sender, error);
                    }
                } else {
                    QString error = olm_session_last_error(session);
                    free(session);
                    return bad_encrypt_str(sender, error);
                }
            }
        } else if (type == 1) {
            if (olm_sessions->contains(sender_key)) {
                int at = 0;
                for (QJsonValue value : olm_sessions->value(sender_key).toObject().value("sessions").toArray()) {
                    QString pickled_session = value.toString();
                    OlmSession* session = (OlmSession*) malloc(olm_session_size());

                    if (session == nullptr) {
                        return bad_encrypt_str(sender, "Cannot allocate memory");
                    }

                    session = olm_session(session);
                    size_t status = olm_unpickle_session(session, m_user_id.toUtf8().data(), m_user_id.toUtf8().length(), pickled_session.toUtf8().data(), pickled_session.toUtf8().length());
                    if (status != olm_error()) {
                        size_t decrypted_buffer_length = olm_decrypt_max_plaintext_length(session, 1, body.toUtf8().data(), body.toUtf8().length());

                        if (decrypted_buffer_length == olm_error()) {
                            qWarning() << olm_session_last_error(session);
                            free(session);
                            continue;
                        }
                        char* decrypted_buffer = (char*) malloc(decrypted_buffer_length);

                        if (decrypted_buffer == nullptr) {
                            free(session);
                            return bad_encrypt_str(sender, "Cannot allocate memory");
                        }

                        size_t str_len = olm_decrypt(session, 1, body.toUtf8().data(), body.toUtf8().length(), decrypted_buffer, decrypted_buffer_length);

                        if (str_len == olm_error()) {
                            qWarning() << olm_session_last_error(session);
                            free(decrypted_buffer);
                            free(session);
                            continue;
                        }

                        // save the session
                        char* session_pickle = (char*) malloc(olm_pickle_session_length(session));

                        if (session_pickle == nullptr) {
                            free(session);
                            free(decrypted_buffer);
                            return bad_encrypt_str(sender, "Cannot allocate memory");
                        }

                        size_t pickle_len = olm_pickle_session(session, m_user_id.toUtf8().data(), m_user_id.toUtf8().length(), session_pickle, olm_pickle_session_length(session));

                        QJsonArray arr = olm_sessions->value(sender_key).toObject().value("sessions").toArray();
                        arr.insert(at, QString::fromUtf8(session_pickle, pickle_len));

                        QJsonObject obj = olm_sessions->value(sender_key).toObject();
                        obj.insert("sessions", arr);


                        QString decrypted = QString::fromUtf8(decrypted_buffer, str_len);
                        olm_sessions->insert(sender_key, obj);
                        free(session);
                        free(session_pickle);
                        free(decrypted_buffer);

                        return decrypted;
                    }
                    at++;
                }


                return bad_encrypt_str(sender, "Cannot find olm session");
            }

            return bad_encrypt_str(sender, "Cannot find olm session");
        }
    } else if (algorithm == QString("m.megolm.v1.aes-sha2")) {
        if (megolm_sessions->contains(session_id)) {
            QJsonObject session_obj = megolm_sessions->value(session_id).toObject();
            // TODO verify sender_key

            OlmInboundGroupSession* session = (OlmInboundGroupSession*) malloc(olm_inbound_group_session_size());

            if (session == nullptr) {
                return bad_encrypt_str(sender, "Cannot allocate session memory");
            }
            session = olm_inbound_group_session(session);
            size_t pickle_status = olm_unpickle_inbound_group_session(session, m_user_id.toUtf8().data(), m_user_id.toUtf8().length(), session_obj.value("pickle").toString().toUtf8().data(), session_obj.value("pickle").toString().toUtf8().length());

            if (pickle_status != olm_error()) {
                int size_of_decrypted = olm_group_decrypt_max_plaintext_length(session, (unsigned char*) encrypted.toString().toUtf8().data(), encrypted.toString().toUtf8().length());
                unsigned char* decrypted_arr = (unsigned char*) malloc(size_of_decrypted);

                if (decrypted_arr == nullptr) {
                    free(session);
                    return bad_encrypt_str(sender, "Cannot allocate memory");
                }
                size_t length_of_decrypted = olm_group_decrypt(session,(unsigned char*) encrypted.toString().toUtf8().data(), encrypted.toString().toUtf8().length(), decrypted_arr, size_of_decrypted, message_index);

                if (length_of_decrypted == olm_error()) {
                    QString error= olm_inbound_group_session_last_error(session);
                    free(session);
                    free(decrypted_arr);
                    return bad_encrypt_str(sender, "Cannot decode: " + error);
                }

                QString decrypted_str = QString::fromUtf8((char *)decrypted_arr, length_of_decrypted);
                QJsonDocument decrypted_json = QJsonDocument::fromJson(decrypted_str.toUtf8());
                QJsonObject decrypted_obj = decrypted_json.object();
                decrypted_obj.insert("sender", sender);

                decrypted_json.setObject(decrypted_obj);

                free(session);
                free(decrypted_arr);

                return decrypted_json.toJson();
            } else {
                QString error = olm_inbound_group_session_last_error(session);
                free(session);
                return bad_encrypt_str(sender, "Unable to unpickle inbound group session: " + error);
            }
        } else {
            return bad_encrypt_str(sender, "Megolm session not found");
        }
    }

    return bad_encrypt_str(sender, "Unable to decrypt - no options: algorithm: " + algorithm);
}

void write_olm_sessions(QJsonObject sessions, Crypto::CryptoManager* manager, Crypto::Key key) {
    QFile file(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/org.yeheng/sailtrix/olm_sessions.aes");
    if (file.open(QFile::WriteOnly)) {
        QJsonDocument doc;
        doc.setObject(sessions);
        QByteArray encrypted = encrypt_bytes(manager, key, doc.toJson());
        file.write(encrypted);
        file.close();
    }
}

void write_megolm_sessions(QJsonObject sessions, Crypto::CryptoManager* manager, Crypto::Key key) {
    QFile file(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/org.yeheng/sailtrix/megolm_sessions.aes");
    if (file.open(QFile::WriteOnly)) {
        QJsonDocument doc;
        doc.setObject(sessions);
        QByteArray encrypted = encrypt_bytes(manager, key, doc.toJson());
        file.write(encrypted);
        file.close();
    }
}

QJsonObject read_olm_sessions(Crypto::CryptoManager* manager, Crypto::Key key) {
    QFile file(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/org.yeheng/sailtrix/olm_sessions.aes");
    if (file.open(QFile::ReadOnly)) {
        QByteArray decrypted = decrypt_bytes(manager, key, file.readAll());
        QJsonDocument from_file = QJsonDocument::fromJson(decrypted);
        file.close();

        return from_file.object();
    }
    return QJsonObject();
}

QJsonObject read_megolm_sessions(Crypto::CryptoManager* manager, Crypto::Key key) {
    QFile file(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/org.yeheng/sailtrix/megolm_sessions.aes");
    if (file.open(QFile::ReadOnly)) {
        QByteArray decrypted = decrypt_bytes(manager, key, file.readAll());
        QJsonDocument from_file = QJsonDocument::fromJson(decrypted);
        file.close();

        return from_file.object();
    }
    return QJsonObject();
}

bool verify_signed_json(QJsonObject json, QString expected_sender, QString expected_device_id, QString verification_key) {
    QJsonObject signatures = json.value("signatures").toObject();
    QJsonValue signatures_of_user = signatures.value(expected_sender);
    if (signatures_of_user.isUndefined()) {
        return false;
    }

    QJsonObject signatures_obj = signatures_of_user.toObject();
    int i = 0;

    OlmUtility* util = (OlmUtility*) malloc(olm_utility_size());
    if (util == nullptr) {
        qFatal("Cannot allocate memory for olm utility");
        return false;
    }

    util = olm_utility(util);

    for (QJsonValue single_sig : signatures_obj) {
        QString key = signatures_obj.keys().at(i);
        QString the_sig = single_sig.toString();
        if (key == (QString("ed25519:") + expected_device_id)) {
            QJsonDocument doc;
            doc.setObject(json);
            QString verify_against = canonical_json(doc);

            size_t verify_result = olm_ed25519_verify(util,
                                                      verification_key.toUtf8().data(),
                                                      verification_key.toUtf8().length(),
                                                      verify_against.toUtf8().data(),
                                                      verify_against.toUtf8().length(),
                                                      the_sig.toUtf8().data(),
                                                      the_sig.toUtf8().length());
            if (verify_result != olm_error()) {
                free(util);
                return true;
            } else {
                free(util);
                return false;
            }
        }
        i++;
    }
    free(util);
    return false; // Err to the side of caution
}

QString canonical_json(QJsonDocument doc) {
    QJsonObject obj = doc.object();
    obj.remove("signatures");
    obj.remove("unsigned");
    doc.setObject(obj);

    return doc.toJson(QJsonDocument::Compact);
}

void save_account(OlmAccount* account, QString user_id, Crypto::CryptoManager* manager, Crypto::Key key) {
    uint8_t* pickle_jar = (uint8_t*) malloc(olm_pickle_account_length(account));

    if (pickle_jar == nullptr) {
        qFatal("Cannot allocate memory for pickle jar");
        return;
    }

    size_t pickle_size = olm_pickle_account(account, user_id.toUtf8().data(), user_id.toUtf8().size(), pickle_jar, olm_pickle_account_length(account));

    QString pickled = QString::fromUtf8((char*) pickle_jar, pickle_size);
    QJsonDocument pickle_doc;
    QJsonObject pickle_obj;
    pickle_obj.insert("pickle", pickled);
    pickle_doc.setObject(pickle_obj);

    QByteArray encrypted = encrypt_bytes(manager, key, pickle_doc.toJson());

    if (encrypted == nullptr) {
        qWarning() << "Cannot encrypt";
        return;
    }

    QFile pickle_file(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/org.yeheng/sailtrix/pickle-jar.aes");
    if (pickle_file.open(QFile::WriteOnly)) {
        pickle_file.write(encrypted);
        pickle_file.close();
    } else {
        qWarning() << "Cannot save pickle jar";
    }

    free(pickle_jar);
}

QString get_olm_plaintext(QJsonObject obj, QString sender, QString recipient, QString recipient_ed25519, QString sender_ed25519) {
    obj.insert("sender", sender);
    obj.insert("recipient", recipient);
    QJsonObject recipient_keys;
    recipient_keys.insert("ed25519", recipient_ed25519);
    obj.insert("recipient_keys", recipient_keys);
    QJsonObject sender_keys;
    sender_keys.insert("ed25519", sender_ed25519);
    obj.insert("keys", sender_keys);

    QJsonDocument doc;
    doc.setObject(obj);

    return doc.toJson(QJsonDocument::Compact);
}

QString get_uuid() {
    return QUuid::createUuid().toString().remove("{").remove("}");
}

QByteArray file_dec2(Crypto::CryptoManager* manager, QByteArray ciphertext, QByteArray key, QByteArray iv) {
       Crypto::DecryptRequest req;
       Crypto::Key key_obj;
       key_obj.setSecretKey(key);
       key_obj.setSize(256);
       key_obj.setAlgorithm(Crypto::CryptoManager::AlgorithmAes);
       req.setKey(key_obj);
       req.setCryptoPluginName(Crypto::CryptoManager::DefaultCryptoStoragePluginName);
       req.setInitializationVector(iv);
       req.setData(ciphertext);
       req.setBlockMode(Crypto::CryptoManager::BlockModeCtr);
       req.setPadding(Crypto::CryptoManager::EncryptionPaddingNone);
       req.setManager(manager);
       req.startRequest();
       req.waitForFinished();

       if (req.result() != Crypto::Result::Succeeded) {
           qWarning() << "Decrypt failed:" << req.result().errorCode() << req.result().errorMessage();
           return nullptr;
       }

       return req.plaintext();
}


Secrets::Secret::Identifier config_secret_id() {
    return Secrets::Secret::Identifier(QStringLiteral("Config"), QStringLiteral("SailtrixWallet2"), Sailfish::Secrets::SecretManager::DefaultEncryptedStoragePluginName);
}

bool init_secrets(Secrets::SecretManager *m_secretManager, Secrets::Secret::Identifier id) {
    Sailfish::Secrets::CreateCollectionRequest createCollection;
    createCollection.setManager(m_secretManager);
    createCollection.setCollectionLockType(Sailfish::Secrets::CreateCollectionRequest::DeviceLock);
    createCollection.setDeviceLockUnlockSemantic(Sailfish::Secrets::SecretManager::DeviceLockKeepUnlocked);
    createCollection.setAccessControlMode(Sailfish::Secrets::SecretManager::OwnerOnlyMode);
    createCollection.setUserInteractionMode(Sailfish::Secrets::SecretManager::SystemInteraction);
    createCollection.setCollectionName(id.collectionName());
    createCollection.setStoragePluginName(Secrets::SecretManager::DefaultEncryptedStoragePluginName);
    createCollection.setEncryptionPluginName(Secrets::SecretManager::DefaultEncryptedStoragePluginName);
    createCollection.startRequest();
    createCollection.waitForFinished();

    if (createCollection.result().code() != Secrets::Result::Succeeded) {
        qWarning() << "Failed to create collection: " << createCollection.result().errorCode() << createCollection.result().errorMessage();
        return false;
    }

    return true;
}

bool store_secret(Secrets::SecretManager* manager, QByteArray& to_store, Secrets::Secret::Identifier id) {
    Secrets::Secret secret(id);
    secret.setData(to_store);

    Secrets::StoreSecretRequest req;
    req.setManager(manager);
    req.setSecretStorageType(Secrets::StoreSecretRequest::CollectionSecret);
    req.setUserInteractionMode(Secrets::SecretManager::SystemInteraction);
    req.setAccessControlMode(Secrets::SecretManager::OwnerOnlyMode);
    req.setSecret(secret);
    req.startRequest();
    req.waitForFinished();

    if (req.result().code() != Secrets::Result::Succeeded) {
        qWarning() << "failed to store secret:" << req.result().errorCode() << req.result().errorMessage();
        return false;
    }

    return true;
}

QByteArray get_secret(Secrets::SecretManager* manager, Secrets::Secret::Identifier id) {
    Secrets::StoredSecretRequest req;
    req.setManager(manager);
    req.setUserInteractionMode(Secrets::SecretManager::SystemInteraction);
    req.setIdentifier(id);
    req.startRequest();
    req.waitForFinished();

    if (req.result().code() == Secrets::Result::Succeeded) {
        return req.secret().data();
    } else {
        return nullptr;
    }
}

bool delete_secret(Secrets::SecretManager* manager, Secrets::Secret::Identifier id) {
    Secrets::DeleteSecretRequest req;
    req.setManager(manager);
    req.setIdentifier(id);
    req.setUserInteractionMode(Secrets::SecretManager::SystemInteraction);
    req.startRequest();
    req.waitForFinished();

    if (req.result().code() == Secrets::Result::Succeeded) {
        return true;
    } else {
        return false;
    }
}


bool delete_collection(Secrets::SecretManager* manager, QLatin1String collection_name) {
    Secrets::DeleteCollectionRequest req;
    req.setManager(manager);
    req.setStoragePluginName(Secrets::SecretManager::DefaultEncryptedStoragePluginName);
    req.setCollectionName(collection_name);
    req.setUserInteractionMode(Secrets::SecretManager::SystemInteraction);
    req.startRequest();
    req.waitForFinished();

    if (req.result().code() == Secrets::Result::Succeeded) {
        return true;
    } else {
        qWarning() << req.result().errorCode() << req.result().errorMessage();
        return false;
    }
}

Crypto::Key::Identifier file_key() {
    return Crypto::Key::Identifier("FileKey", "SailtrixWallet2", Crypto::CryptoManager::DefaultCryptoStoragePluginName);
}

bool create_key(Crypto::CryptoManager* manager, QString auth_token, Secrets::Secret::Identifier id, Crypto::Key::Identifier key_id) {
    Crypto::Key keyTemplate;
    keyTemplate.setOrigin(Crypto::Key::OriginDevice);
    keyTemplate.setAlgorithm(Crypto::CryptoManager::AlgorithmAes);
    keyTemplate.setOperations(Crypto::CryptoManager::OperationEncrypt | Crypto::CryptoManager::OperationDecrypt);
    keyTemplate.setIdentifier(key_id);

    Crypto::KeyDerivationParameters kdfParams;
    kdfParams.setKeyDerivationFunction(Crypto::CryptoManager::KdfPkcs5Pbkdf2);
    kdfParams.setKeyDerivationMac(Crypto::CryptoManager::MacHmac);
    kdfParams.setKeyDerivationDigestFunction(Crypto::CryptoManager::DigestSha256);
    kdfParams.setIterations(16384);
    kdfParams.setOutputKeySize(256);
    kdfParams.setSalt(QByteArray("org.yeheng.sailtrix"));
    kdfParams.setInputData(auth_token.toUtf8());

    Crypto::GenerateStoredKeyRequest generateKey;
    generateKey.setManager(manager);
    generateKey.setKeyTemplate(keyTemplate);
    generateKey.setKeyDerivationParameters(kdfParams);
    generateKey.setCryptoPluginName(id.storagePluginName());
    generateKey.startRequest();


    generateKey.waitForFinished();

    if (generateKey.result().code() != Crypto::Result::Succeeded) {
        qWarning() << "failed to generate key:" << generateKey.result().errorCode() << generateKey.result().errorMessage();
        return false;
    }

    return true;
}

Crypto::Key get_key(Crypto::CryptoManager* manager, Crypto::Key::Identifier id) {
    Crypto::StoredKeyRequest req;
    req.setIdentifier(id);
    req.setManager(manager);
    req.startRequest();
    req.waitForFinished();

    if (req.result().code() != Crypto::Result::Succeeded) {
        qWarning() << "failed to get key:" << req.result().errorCode() << req.result().errorMessage();
    }

    return req.storedKey();
}

QByteArray encrypt_bytes(Crypto::CryptoManager* manager, Crypto::Key key, QByteArray data) {
    Crypto::GenerateInitializationVectorRequest gen_iv_req;
    gen_iv_req.setManager(manager);
    gen_iv_req.setAlgorithm(Crypto::CryptoManager::AlgorithmAes);
    gen_iv_req.setBlockMode(Crypto::CryptoManager::BlockModeCbc);
    gen_iv_req.setCryptoPluginName(key.storagePluginName());
    gen_iv_req.setKeySize(256);
    gen_iv_req.startRequest();
    gen_iv_req.waitForFinished();

    if (gen_iv_req.result().code() != Crypto::Result::Succeeded) {
        qWarning() << "failed to get IV:" << gen_iv_req.result().errorCode() << gen_iv_req.result().errorMessage();
        return nullptr;
    }

    Crypto::EncryptRequest enc_req;
    enc_req.setManager(manager);
    enc_req.setData(data);
    enc_req.setInitializationVector(gen_iv_req.generatedInitializationVector());
    enc_req.setKey(key);
    enc_req.setBlockMode(Crypto::CryptoManager::BlockModeCbc);
    enc_req.setPadding(Crypto::CryptoManager::EncryptionPaddingNone);
    enc_req.setCryptoPluginName(key.storagePluginName());
    enc_req.startRequest();
    enc_req.waitForFinished();

    if (enc_req.result().code() != Crypto::Result::Succeeded) {
        qWarning() << "failed to encrypt:" << enc_req.result().errorCode() << enc_req.result().errorMessage();
        return nullptr;
    }

    QByteArray final(enc_req.initializationVector().toBase64() + "\n");
    QByteArray ciphertext = enc_req.ciphertext();

    final.append(ciphertext);
    return final;
}

QByteArray decrypt_bytes(Crypto::CryptoManager* manager, Crypto::Key key, QByteArray data) {
    if (!data.contains('\n')) {
        qWarning() << "incorrect format - no newline character found";
        return nullptr;
    }

    QByteArray base_64_iv;
    char c = data.at(0);
    int i = 0;
    while (c != '\n') {
        base_64_iv.append(c);
        c = data.at(++i);
    }

    QByteArray iv = QByteArray::fromBase64(base_64_iv);
    QByteArray encrypted_data = data.mid(i+1);

    Crypto::DecryptRequest req;
    req.setManager(manager);
    req.setData(encrypted_data);
    req.setInitializationVector(iv);
    req.setKey(key);
    req.setPadding(Crypto::CryptoManager::EncryptionPaddingNone);
    req.setBlockMode(Crypto::CryptoManager::BlockModeCbc);
    req.setCryptoPluginName(key.storagePluginName());
    req.startRequest();
    req.waitForFinished();


    if (req.result().code() != Crypto::Result::Succeeded) {
        qWarning() << "failed to decrypt:" << req.result().errorCode() << req.result().errorMessage();
        return nullptr;
    }

    return req.plaintext();
}

QByteArray encrypt_bytes(Crypto::CryptoManager* manager, QByteArray data, QString& key_str, QString& iv_str) {
    Crypto::Key keyTemplate;
    keyTemplate.setOrigin(Crypto::Key::OriginDevice);
    keyTemplate.setAlgorithm(Crypto::CryptoManager::AlgorithmAes);
    keyTemplate.setOperations(Crypto::CryptoManager::OperationEncrypt);

    Crypto::KeyDerivationParameters params;
    params.setKeyDerivationFunction(Crypto::CryptoManager::KdfPkcs5Pbkdf2);
    params.setKeyDerivationMac(Crypto::CryptoManager::MacHmac);
    params.setKeyDerivationDigestFunction(Crypto::CryptoManager::DigestSha512);
    params.setIterations(16384);
    params.setOutputKeySize(256);
    params.setSalt(QByteArray("org.yeheng.sailtrix"));

    uchar* random = new uchar[64];
    SailfishCryptoRAND_bytes(random, 64, manager);
    params.setInputData(QByteArray((char*) random));

    Crypto::GenerateKeyRequest gen_key_req;
    gen_key_req.setManager(manager);
    gen_key_req.setKeyTemplate(keyTemplate);
    gen_key_req.setKeyDerivationParameters(params);
    gen_key_req.setCryptoPluginName(Crypto::CryptoManager::DefaultCryptoStoragePluginName);
    gen_key_req.startRequest();
    gen_key_req.waitForFinished();

    delete[] random;

    if (gen_key_req.result().code() != Sailfish::Crypto::Result::Succeeded) {
        qWarning() << "Failed to generate key" << gen_key_req.result().errorCode() << gen_key_req.result().errorMessage();
        return nullptr;
    }

    Crypto::Key key = gen_key_req.generatedKey();
    key_str = key.secretKey().toBase64(QByteArray::Base64UrlEncoding | QByteArray::OmitTrailingEquals);

    Crypto::GenerateInitializationVectorRequest gen_iv_req;
    gen_iv_req.setManager(manager);
    gen_iv_req.setAlgorithm(Crypto::CryptoManager::AlgorithmAes);
    gen_iv_req.setBlockMode(Crypto::CryptoManager::BlockModeCtr);
    gen_iv_req.setCryptoPluginName(Crypto::CryptoManager::DefaultCryptoStoragePluginName);
    gen_iv_req.setKeySize(256);
    gen_iv_req.startRequest();
    gen_iv_req.waitForFinished();

    if (gen_iv_req.result().code() != Crypto::Result::Succeeded) {
        qWarning() << "Failed to generate IV:" << gen_iv_req.result().errorCode() << gen_iv_req.result().errorMessage();
        return nullptr;
    }

    iv_str = gen_iv_req.generatedInitializationVector().toBase64(QByteArray::OmitTrailingEquals);

    Crypto::EncryptRequest req;
    req.setManager(manager);
    req.setData(data);
    req.setInitializationVector(gen_iv_req.generatedInitializationVector());
    req.setKey(gen_key_req.generatedKey());
    req.setBlockMode(Crypto::CryptoManager::BlockModeCtr);
    req.setPadding(Crypto::CryptoManager::EncryptionPaddingNone);
    req.setCryptoPluginName(Crypto::CryptoManager::DefaultCryptoStoragePluginName);
    req.startRequest();
    req.waitForFinished();

    if (req.result().code() != Crypto::Result::Succeeded) {
        qWarning() << "Failed to encrypt file:" << req.result().errorCode() << req.result().errorMessage();
        return nullptr;
    }

    return req.ciphertext();
}

int SailfishCryptoRAND_bytes(unsigned char* buf, int num, Crypto::CryptoManager* manager) {
    Crypto::GenerateRandomDataRequest request;
    request.setManager(manager);
    request.setCryptoPluginName(Crypto::CryptoManager::DefaultCryptoStoragePluginName);
    request.setCsprngEngineName(QStringLiteral("/dev/urandom"));
    request.setNumberBytes(num);
    request.startRequest();
    request.waitForFinished();
    QByteArray randomBytes = request.generatedData();
    memcpy(buf, randomBytes.data(), num);
    return 1;
}
