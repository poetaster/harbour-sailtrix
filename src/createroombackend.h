#ifndef CREATEROOMBACKEND_H
#define CREATEROOMBACKEND_H

#include <QObject>
#include <QNetworkAccessManager>
#include <Sailfish/Secrets/secretmanager.h>

class CreateRoomBackend : public QObject
{
    Q_OBJECT
public:
    explicit CreateRoomBackend(QObject *parent = nullptr);
    ~CreateRoomBackend();
    Q_INVOKABLE void create_public(QString alias, QString name, QString topic);
    Q_INVOKABLE void create_private(QString name, QString topic, bool encrypt);
    Q_INVOKABLE QString hs();

signals:
    void created(QString room_id);
private slots:
    void done(QNetworkReply* reply);
private:
    QString m_access_token;
    QString m_hs_url;
    QString m_hs;
    Sailfish::Secrets::SecretManager m_secretManager;
    QNetworkAccessManager* manager;

};

#endif // CREATEROOMBACKEND_H
