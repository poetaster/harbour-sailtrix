#ifndef ROOMDIRECTORYMODEL_H
#define ROOMDIRECTORYMODEL_H

#include <QStandardItemModel>

class RoomDirectoryModel : public QStandardItemModel
{
public:
    enum Role {
        room_id=Qt::UserRole,
        name,
        description,
        alias,
        joined_count,
        avatar_url,
        avatar_path
    };
    explicit RoomDirectoryModel(QObject * parent = 0): QStandardItemModel(parent) {}

    explicit RoomDirectoryModel( int rows, int columns, QObject * parent = 0 ): QStandardItemModel(rows, columns, parent) {}

    QHash<int, QByteArray> roleNames() const;
};

#endif // ROOMDIRECTORYMODEL_H
