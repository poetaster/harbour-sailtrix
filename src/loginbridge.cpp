#include <cstdlib>
#include "loginbridge.h"
#include <QDebug>
#include <QUrl>
#include <QNetworkRequest>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QStandardPaths>
#include <QFile>
#include <QDir>
#include <Sailfish/Secrets/secretmanager.h>
#include <Sailfish/Secrets/secret.h>
#include <Sailfish/Secrets/interactionparameters.h>
#include <Sailfish/Secrets/createcollectionrequest.h>
#include <Sailfish/Secrets/storesecretrequest.h>
#include <Sailfish/Secrets/storedsecretrequest.h>
#include "olm/olm.hh"
#include "olm/account.hh"
#include "enc-util.h"


using namespace std;
using namespace Sailfish;

QUrl homeserver_url;

void LoginBridge::login(bool serverDiscoveryOnly) {
    m_serverDiscoveryOnly = serverDiscoveryOnly;
    setError(nullptr);
    qDebug() << "Clicked";
    manager = new QNetworkAccessManager(this);
    connect(manager, &QNetworkAccessManager::finished, this, &LoginBridge::doServerDiscovery);
    keys_upload = new QNetworkAccessManager(this);
    connect(keys_upload, &QNetworkAccessManager::finished, this, &LoginBridge::finishUploadKeys);
    manager->get(QNetworkRequest(QUrl(QString("https://") + m_homeserverUrl + "/.well-known/matrix/client")));
    qDebug() << "After get";
}

QString LoginBridge::homeserverUrl() {
    return m_homeserverUrl;
}

void LoginBridge::setHomeserverUrl(const QString &hsUrl) {
    if (m_homeserverUrl == hsUrl) return;
    m_homeserverUrl = hsUrl;
}

QString LoginBridge::username() {
    return m_username;
}

void LoginBridge::setUsername(const QString &username) {
    if (m_username == username) return;
    m_username = username;
}

QString LoginBridge::password() {
    return m_password;
}

void LoginBridge::setPassword(const QString &password) {
    if (m_password == password) return;
    m_password = password;
}

QString LoginBridge::error() {
    return m_error;
}

void LoginBridge::setError(const QString &error) {
    if (m_error == error) return;
    m_error = error;
    emit errorChanged();
}

void LoginBridge::doServerDiscovery(QNetworkReply *reply) {
    if (reply->error() == QNetworkReply::NoError) {
        QString answer = reply->readAll();
        QJsonDocument document = QJsonDocument::fromJson(answer.toUtf8());
        QJsonObject mainobj = document.object();

        QJsonObject base_url = mainobj.value(QString("m.homeserver")).toObject();
        QString real_homeserver_url = base_url.value("base_url").toString();

        qDebug() << "Discovered homeserver URL: " << real_homeserver_url;

        QUrl raw_hsUrl(real_homeserver_url);
        QUrl hsUrl = raw_hsUrl.adjusted(QUrl::RemovePath);

        if (hsUrl.isValid()) {
            homeserver_url = hsUrl;

            disconnect(manager, &QNetworkAccessManager::finished, this, &LoginBridge::doServerDiscovery);
            connect(manager, &QNetworkAccessManager::finished, this, &LoginBridge::validateHomeserver);
            manager->get(QNetworkRequest(QUrl(QString(hsUrl.toString() + "/_matrix/client/versions"))));
        } else {
            qDebug() << "Discovered Homeserver URL not valid.";
            setError("Discovered Homeserver URL not valid");
        }
    } else {
        qDebug() << "Skipping discovery";
        homeserver_url =  QUrl("https://" + homeserverUrl());
        disconnect(manager, &QNetworkAccessManager::finished, this, &LoginBridge::doServerDiscovery);
        connect(manager, &QNetworkAccessManager::finished, this, &LoginBridge::validateHomeserver);
        manager->get(QNetworkRequest(QUrl(QString(homeserver_url.toString() + "/_matrix/client/versions"))));
    }
}

void LoginBridge::validateHomeserver(QNetworkReply* reply) {
    if (reply->error() == QNetworkReply::NoError) {
        qDebug() << "Homeserver is valid.";

        if (m_serverDiscoveryOnly) {
            setHomeserverUrl(homeserver_url.toString());
            emit discovered();
        } else {
            disconnect(manager, &QNetworkAccessManager::finished, this, &LoginBridge::validateHomeserver);
            connect(manager, &QNetworkAccessManager::finished, this, &LoginBridge::validateFlow);
            manager->get(QNetworkRequest(QUrl(QString(homeserver_url.toString() + "/_matrix/client/r0/login"))));
        }
    } else {
        qDebug() << "Homeserver validation error: " << reply->errorString();
        setError("Homeserver is not valid");
    }
}

void LoginBridge::validateFlow(QNetworkReply* reply) {
    if (reply->error() == QNetworkReply::NoError) {
        QString response = reply->readAll();
        QJsonDocument document = QJsonDocument::fromJson(response.toUtf8());
        QJsonObject obj = document.object();
        QJsonArray flows = obj.value("flows").toArray();
        bool foundPasswordLogin = false;

        for (int i = 0; i < flows.size(); i++) {
             QJsonObject o = flows.at(i).toObject();
             if (o.value("type").toString() == QString("m.login.password")) {
                 foundPasswordLogin = true;
                 break;
             }
        }

        if (foundPasswordLogin) {
            disconnect(manager, &QNetworkAccessManager::finished, this, &LoginBridge::validateFlow);
            connect(manager, &QNetworkAccessManager::finished, this, &LoginBridge::doLogin);

            QJsonObject obj;
            obj.insert("type", "m.login.password");

            QJsonObject identifier;
            identifier.insert("type", "m.id.user");
            identifier.insert("user", username());

            obj.insert("identifier", identifier);
            obj.insert("password", password());

            obj.insert("initial_device_display_name", "Sailtrix on SailfishOS");

            QNetworkRequest loginRequest(QString(homeserver_url.toString() + "/_matrix/client/r0/login"));
            loginRequest.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

            manager->post(loginRequest, QJsonDocument(obj).toJson());
        } else {
            setError("Homeserver does not support password login");
        }
    }
}

void LoginBridge::ssoLogin(QString token) {
    disconnect(manager, &QNetworkAccessManager::finished, this, &LoginBridge::validateHomeserver);
    connect(manager, &QNetworkAccessManager::finished, this, &LoginBridge::doLogin);

    QJsonObject obj;
    obj.insert("type", "m.login.token");

    obj.insert("token", token);

    obj.insert("initial_device_display_name", "Sailtrix on SailfishOS");

    QNetworkRequest loginRequest(QString(homeserver_url.toString() + "/_matrix/client/r0/login"));
    loginRequest.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    manager->post(loginRequest, QJsonDocument(obj).toJson());
}

void LoginBridge::doLogin(QNetworkReply* reply) {
    qDebug() << "Login responded: " << reply->error();

    if (reply->error() == QNetworkReply::NoError) {

            QJsonDocument document;
            QJsonObject obj;


            QJsonDocument fromServer = QJsonDocument::fromJson(reply->readAll());

            obj.insert("access_token", QJsonValue(fromServer.object().value("access_token").toString()));
            obj.insert("home_server", homeserver_url.toString());
            obj.insert("user_id", fromServer.object().value("user_id").toString());
            obj.insert("device_id", fromServer.object().value("device_id").toString());
            obj.insert("homeserver", m_homeserverUrl);

            qDebug() << "Setting homeserver to" << m_homeserverUrl;
            document.setObject(obj);

            Secrets::Secret::Identifier id = config_secret_id();
            QByteArray doc_arr = document.toJson();

            if (!init_secrets(&m_secretManager, id)) {
                setError("Cannot create secret collection");
                return;
            }

            if (!store_secret(&m_secretManager, doc_arr, id)) {
                setError("Cannot store access token in secure storage");
                return;
            }

            QDir config_dir(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/org.yeheng");
            config_dir.mkpath("sailtrix");


            QDir cache(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/org.yeheng");
            cache.mkpath("sailtrix");

            if (!create_key(&m_cryptoManager, fromServer.object().value("access_token").toString(), id, file_key())) {
                setError("Cannot create file encryption key");
                return;
            } else {
                qDebug() << "Created file encryption key";
            }

            OlmAccount* account_buffer = (OlmAccount*) std::malloc(olm_account_size());
            if (account_buffer == nullptr) {
                qFatal("Cannot allocate memory for olm account");
            }
            OlmAccount* account = olm_account(account_buffer);

            unsigned char* random = (unsigned char*) std::malloc(olm_create_account_random_length(account));

            if (random == nullptr) {
                qFatal("Cannot allocate memory for random buffer");
            }

            SailfishCryptoRAND_bytes(random, olm_create_account_random_length(account), &m_cryptoManager);

            size_t err = olm_create_account(account, random, olm_create_account_random_length(account));

            if (err == olm_error()) {
                setError(olm_account_last_error(account));
                free(account_buffer);
                free(random);
                return;
            }

            uint8_t* arr = (uint8_t*) malloc(olm_account_identity_keys_length(account));

            if (arr == nullptr) {
                qFatal("Cannot allocate memory for id keys");
            }

            size_t string_length = olm_account_identity_keys(account, arr, olm_account_identity_keys_length(account));

            QString keys_str = QString::fromUtf8((char*) arr, string_length);

            QFile encryption_keys_file(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/org.yeheng/sailtrix/identity_keys.json");
            if (encryption_keys_file.open(QFile::WriteOnly)) {
                encryption_keys_file.write(keys_str.toUtf8());
                encryption_keys_file.close();
            } else {
                setError("Cannot write identity keys");
                free(arr);
                free(account_buffer);
                free(random);
                return;
            }

            int amount_keys_needed = olm_account_max_number_of_one_time_keys(account) / 2;
            unsigned char* random_for_otk = (unsigned char*) std::malloc(olm_account_generate_one_time_keys_random_length(account, amount_keys_needed));

            if (random_for_otk == nullptr) {
                qFatal("Cannot allocate memory for random buffer");
            }

            SailfishCryptoRAND_bytes(random_for_otk, olm_account_generate_one_time_keys_random_length(account, amount_keys_needed), &m_cryptoManager);
            size_t result = olm_account_generate_one_time_keys(account, amount_keys_needed, random_for_otk, olm_account_generate_one_time_keys_random_length(account, amount_keys_needed));

            if (result == olm_error()) {
                setError(olm_account_last_error(account));
                free(account);
                free(random_for_otk);
                free(random);
                free(arr);
                return;
            }

            uint8_t* otk_json = (uint8_t*) std::malloc(olm_account_one_time_keys_length(account));

            if (otk_json == nullptr) {
                qFatal("Cannot allocate memory for OTK buffer");
            }

            size_t otk_str_len = olm_account_one_time_keys(account, otk_json, olm_account_one_time_keys_length(account));

            if (otk_str_len == olm_error()) {
                setError(olm_account_last_error(account));
                free(account);
                free(random_for_otk);
                free(random);
                free(arr);
                free(otk_json);
                return;
            }

            QString otk_str = QString::fromUtf8((char*) otk_json, otk_str_len);

            QString user_id = fromServer.object().value("user_id").toString();

            QJsonDocument doc;
            QJsonObject master_obj;
            QJsonObject device_keys_obj;

            device_keys_obj.insert("user_id", fromServer.object().value("user_id").toString());
            device_keys_obj.insert("device_id", fromServer.object().value("device_id").toString());

            QJsonArray algorithms;
            algorithms.append("m.olm.v1.curve25519-aes-sha2");
            algorithms.append("m.megolm.v1.aes-sha2");

            device_keys_obj.insert("algorithms", algorithms);

            QString device_id = fromServer.object().value("device_id").toString();

            QJsonDocument identity_json = QJsonDocument::fromJson(keys_str.toUtf8());

            QJsonObject keys;
            keys.insert("curve25519:" + device_id, identity_json.object().value("curve25519").toString());
            keys.insert("ed25519:" + device_id, identity_json.object().value("ed25519").toString());

            device_keys_obj.insert("keys", keys);

            QJsonObject signatures;
            QJsonObject signature;

            uint8_t* signed_message = (uint8_t*) std::malloc(olm_account_signature_length(account));

            if (signed_message == nullptr) {
                qFatal("Cannot allocate memory for signed message");
            }

            QJsonDocument to_sign_dk;
            to_sign_dk.setObject(device_keys_obj);
            QString dk_str_to_sign = to_sign_dk.toJson(QJsonDocument::Compact);

            size_t signature_length = olm_account_sign(account, (unsigned char*) dk_str_to_sign.toUtf8().data(), dk_str_to_sign.toUtf8().length(), signed_message, olm_account_signature_length(account));

            if (signature_length == olm_error()) {
                setError(olm_account_last_error(account));
                free(account);
                free(random_for_otk);
                free(random);
                free(arr);
                free(otk_json);
                free(signed_message);
                return;
            }

            signature.insert("ed25519:" + device_id, QString::fromUtf8((char*) signed_message, signature_length));
            signatures.insert(fromServer.object().value("user_id").toString(), signature);
            device_keys_obj.insert("signatures", signatures);

            master_obj.insert("device_keys", device_keys_obj);

            QJsonObject one_time_keys_obj;
            QJsonDocument otk_doc = QJsonDocument::fromJson(otk_str.toUtf8());

            int k = 0;
            auto otk_ids = otk_doc.object().value("curve25519").toObject().keys();
            for (QJsonValue value : otk_doc.object().value("curve25519").toObject()) {
                QString key_id = otk_ids.at(k);
                QString the_key = value.toString();

                QJsonDocument to_sign;
                QJsonObject sign_obj;
                sign_obj.insert("key", the_key);
                to_sign.setObject(sign_obj);

                QString sign_message = to_sign.toJson(QJsonDocument::Compact);

                uint8_t* signed_key = (uint8_t*) std::malloc(olm_account_signature_length(account));

                if (signed_key == nullptr) {
                    qFatal("Cannot allocate memory for key");
                }

                size_t signed_key_length = olm_account_sign(account, (unsigned char*) sign_message.toUtf8().data(), sign_message.length(), signed_key, olm_account_signature_length(account));

                if (signed_key_length == olm_error()) {
                    setError(olm_account_last_error(account));
                    free(account);
                    free(random_for_otk);
                    free(random);
                    free(arr);
                    free(otk_json);
                    free(signed_message);
                    free(signed_key);
                    return;
                }

                QJsonObject new_obj;
                new_obj.insert("key", the_key);

                QJsonObject signatures_obj;
                QJsonObject signature_obj;

                signature_obj.insert("ed25519:" + device_id, QString::fromUtf8((char*) signed_key, signed_key_length));
                signatures_obj.insert(user_id, signature_obj);

                new_obj.insert("signatures", signatures_obj);
                one_time_keys_obj.insert("signed_curve25519:" + key_id, new_obj);

                free(signed_key);
                k++;
            }

            master_obj.insert("one_time_keys", one_time_keys_obj);

            doc.setObject(master_obj);

            // Do tests

            OlmUtility* util = (OlmUtility*) std::malloc(olm_utility_size());

            if (util == nullptr) {
                qFatal("Cannot allocate memory for olm utility");
                return;
            }

            olm_utility(util);

            olm_account_mark_keys_as_published(account);


            save_account(account, user_id, &m_cryptoManager, get_key(&m_cryptoManager, file_key()));


            QNetworkRequest req(QUrl(homeserver_url.toString() + "/_matrix/client/r0/keys/upload"));
            req.setRawHeader(QByteArray("Authorization"), QString(QString("Bearer ") + fromServer.object().value("access_token").toString()).toUtf8());
            req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

            keys_upload->post(req, doc.toJson());

            keys_str = QString("");

            free(account);
            free(random_for_otk);
            free(random);
            free(arr);
            free(otk_json);
            free(signed_message);

            qDebug() << "Done creating encryption keys";
       //  }
    } else {
        setError(reply->errorString());
    }
}

void LoginBridge::finishUploadKeys(QNetworkReply* reply) {
    if (reply->error() == QNetworkReply::NoError) {
        qDebug() << "Done uploading encryption keys";
        setError("Done");
    } else {
        setError("Incorrect credentials");
    }
}

