#include "invitebackend.h"
#include "enc-util.h"
#include <QNetworkRequest>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QFile>
#include <QStandardPaths>
#include <QJsonArray>

InviteBackend::InviteBackend(QObject *parent) : QObject(parent), joiner { new QNetworkAccessManager(this) }, rejector { new QNetworkAccessManager(this) }, account_data_sender { new QNetworkAccessManager(this)}, m_user_to_dm { QString() }
{
    connect(joiner, &QNetworkAccessManager::finished, this, &InviteBackend::joined);
    connect(rejector, &QNetworkAccessManager::finished, this, &InviteBackend::rejected);
    connect(account_data_sender, &QNetworkAccessManager::finished, this, &InviteBackend::accountDataSent);
}

void InviteBackend::join(QString room_id) {
    QByteArray secret_stored = get_secret(&m_secretManager, config_secret_id());
    if (secret_stored != nullptr) {
        QJsonDocument document = QJsonDocument::fromJson(secret_stored);
        m_access_token = document.object().value("access_token").toString();
        m_hs_url = document.object().value("home_server").toString();
        m_user_id = document.object().value("user_id").toString();


        QNetworkRequest req(m_hs_url + "/_matrix/client/r0/rooms/" + room_id + "/join");
        req.setRawHeader(QByteArray("Authorization"), (QString("Bearer " + m_access_token).toUtf8()));
        req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

        m_user_to_dm = "";

        joiner->post(req, "{}");
    }
}

void InviteBackend::join_direct(QString room_id, QString uid) {
    QByteArray secret_stored = get_secret(&m_secretManager, config_secret_id());
    if (secret_stored != nullptr) {
        QJsonDocument document = QJsonDocument::fromJson(secret_stored);
        m_access_token = document.object().value("access_token").toString();
        m_hs_url = document.object().value("home_server").toString();
        m_user_id = document.object().value("user_id").toString();


        QNetworkRequest req(m_hs_url + "/_matrix/client/r0/rooms/" + room_id + "/join");
        req.setRawHeader(QByteArray("Authorization"), (QString("Bearer " + m_access_token).toUtf8()));
        req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

        m_user_to_dm = uid;

        joiner->post(req, "{}");
    }
}

void InviteBackend::reject(QString room_id) {
    QByteArray secret_stored = get_secret(&m_secretManager, config_secret_id());
    if (secret_stored != nullptr) {
        QJsonDocument document = QJsonDocument::fromJson(secret_stored);
        m_access_token = document.object().value("access_token").toString();
        m_hs_url = document.object().value("home_server").toString();
        m_user_id = document.object().value("user_id").toString();


        QNetworkRequest req(m_hs_url + "/_matrix/client/r0/rooms/" + room_id + "/leave");
        req.setRawHeader(QByteArray("Authorization"), (QString("Bearer " + m_access_token).toUtf8()));
        req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

        rejector->post(req, "{}");
    }
}

void InviteBackend::joined(QNetworkReply* reply) {
    if (reply->error() == QNetworkReply::NoError) {
        QJsonDocument doc = QJsonDocument::fromJson(reply->readAll());
        QString m_new_room_id = doc.object().value("room_id").toString();

        if (!m_user_to_dm.isEmpty()) {
            QJsonObject dms;
            QFile direct_file(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/org.yeheng/sailtrix/direct.json");
            if (direct_file.open(QFile::ReadOnly)) {
                dms = QJsonDocument::fromJson(direct_file.readAll()).object();
                direct_file.close();
            }

            QJsonArray arr = dms.value(m_user_to_dm).toArray();
            arr.append(m_new_room_id);

            dms.insert(m_user_to_dm, arr);

            if (direct_file.open(QFile::WriteOnly)) {
                direct_file.write(QJsonDocument(dms).toJson());
                direct_file.close();
            }

            QNetworkRequest req(m_hs_url + "/_matrix/client/r0/user/" + m_user_id + "/account_data/m.direct");
            req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
            req.setRawHeader(QByteArray("Authorization"), QString("Bearer " + m_access_token).toUtf8());

            account_data_sender->put(req, QJsonDocument(dms).toJson());
        } else {
            emit joinDone();
        }
    } else {
        emit joinError();
    }
}

void InviteBackend::rejected(QNetworkReply* reply) {
    if (reply->error() == QNetworkReply::NoError) {
        emit rejectDone();
    }
}

void InviteBackend::accountDataSent(QNetworkReply* reply) {
    if (reply->error() == QNetworkReply::NoError) {
        emit joinDone();
    } else {
        emit joinError();
    }
}
