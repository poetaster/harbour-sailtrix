#include "roomsmodel.h"

QHash<int, QByteArray> RoomsModel::roleNames() const {
    QHash<int, QByteArray> list;
    list[name] = "name";
    list[icon] = "avatar";
    list[firstMessage] = "latestMessage";
    list[unreadMessages] = "unread";
    list[roomId] = "rid";
    list[is_invite] = "is_invite";
    list[section_name] = "section_name";
    list[is_direct] = "is_direct";
    list[timestamp] = "timestamp";

    return list;
}
